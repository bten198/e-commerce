const express = require('express');
const User = require('../models/User');

const router = express.Router();

router.get('/', async (req, res) => {
    try{
        const users = await User.find();
        res.json(users);
    } catch(err) {
        res.json({message: err});
    }
});

router.post('/', async (req, res) => {
    const user = new User({
        userEmailId: req.body.userEmailId,
        userPassword: req.body.userPassword,
        userCartItems: req.body.userCartItems,
        userAccessLevel: req.body.userAccessLevel,
        userOrders: req.body.userOrders,
        type: req.body.type
    });

    try {
        const save = await user.save();
        res.json(save);
    } catch(err) {
        res.json({message: err});
    }
})

module.exports = router;