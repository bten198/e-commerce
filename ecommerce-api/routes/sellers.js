const express = require('express');
const Seller = require('../models/Seller');
const Product = require('../models/Product');

const router = express.Router();

router.get("/", async (req, res) => {
    try{
        const sellers = await Seller.find();
        res.json(sellers);
    }catch(err){
        res.send("Something Went Wrong");
    }
});

router.post("/", async (req,res) => {
        const seller = new Seller({
            sellerName: req.body.sellerName,
            sellerAddress: req.body.sellerAddress,
            sellerContactNo: req.body.sellerContactNo,
            isSellerApproved: req.body.isSellerApproved,
            sellerEmailId: req.body.sellerEmailId,
            sellerPassword: req.body.sellerPassword,
            sellerRegistrationDate: req.body.sellerRegistrationDate,
        })
        seller.save()
        .then(data => res.json(data))
        .catch(err => res.json(err));
});

router.patch("/:sellerId", async (req, res) => {
    const { sellerId } = req.params;
    const date = new Date();
    try{
        const seller = await Seller.updateOne({"sellerEmailId": sellerId},
        {$set: {
            isSellerApproved: true,
            sellerApprovedDate: date.toISOString(),
        }});
        res.json(seller);
    }
    catch (err) {
        res.json(err);
    }
})

router.get("/:sellerEmail", async (req,res) => {
    const {sellerEmail} = req.params;
    const seller = await Seller.find({sellerEmailId: sellerEmail});
    console.log(res.json(seller));
});

router.delete('/:id', async (req, res) => {
    Seller.deleteOne({sellerEmailId: req.params.id})
    .then(result => res.json({status: "Success"}))
    .catch(err => res.sendStatus(404));

    Product.deleteMany({sellerId: req.params.id})
    .then(result => res.json({status: "Success"}))
    .catch(err => console.log("Getting Error in deleting Products"))
});

module.exports = router;