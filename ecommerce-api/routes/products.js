const express = require('express');
const Product = require('../models/Product');

const router = express.Router();

router.get('/', async (req,res) => {
    try{
        const products = await Product.find();
        res.json(products);
    } catch(err) {
        res.send({message: "Something Went Wrong!!!!"});
    }
    
});

router.get('/:id', async (req, res) => {
    const id = req.params.id;
    try {
        let products;
        if(Number(id)){
        products = await Product.find({
            productId: id
        });
        } else {
        products = await Product.find({
            sellerId: id
        });
    }
        res.json(products);
    } catch(error){
        res.send({message: error});
    }
});

router.patch("/:id/:quantity", async (req, res) => {
    const { id, quantity } = req.params;
    try{
        const product = await Product.updateOne({"productId": id},
        {$set: {
            availableStock: quantity
        }});
        res.json(product);
    }
    catch (err) {
        res.json(err);
    }
})

router.post('/', (req, res) => {
    const product = new Product({
        productId: req.body.productId,
        productName: req.body.productName,
        productPrice: req.body.productPrice,
        availableStock: req.body.availableStock,
        productShortDescription: req.body.productShortDescription,
        productDescription: req.body.productDescription,
        sellerName: req.body.sellerName,
        sellerId: req.body.sellerId,

    });
    product.save()
    .then(data => res.json(data))
    .catch(err => res.json({message: err}));
})

router.delete('/:id', async (req, res) => {
    Product.deleteOne({_id: req.params.id})
    .then(result => res.json({status: "Success"}))
    .catch(err => res.sendStatus(404));
});

module.exports = router;