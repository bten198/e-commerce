const express = require('express');
const Sales = require('../models/Sales');

const router = express.Router();

router.post('/', async (req, res) => {
    const sale = new Sales({
        saleDate: req.body.saleDate,
        productId: req.body.productId,
        productName: req.body.productName,
        productPrice: req.body.productPrice,
        sellerName: req.body.sellerName,
        sellerId: req.body.sellerId,
        quantity: req.body.quantity,
        type: req.body.type,
    });

    sale.save()
    .then(data => res.json(data))
    .catch(err => console.log("Something Went Wrong"));
});

router.get('/', async (req, res) => {
    try{
        const sales = Sales.find();
        res.json(sales);
    }
    catch(error){
        res.json({error: "Something Went Wrong"});
    }
});

router.get('/:sellerId', async (req, res) => {
    try {
        const sales = await Sales.find({
            sellerId: req.params.sellerId
        });
        res.json(sales);
    } catch(error){
        res.send({message: error});
    }
});

module.exports = router;