const express = require('express');
const mongoose = require('mongoose');
const env = require('dotenv');
const cors = require('cors');

env.config();
//Import Routes
const productRoute = require('./routes/products');
const userRoute = require('./routes/users');
const sellerRoute = require('./routes/sellers');
const salesRoute = require('./routes/sales');

const app = express();

//Middlewares
// app.use('/posts', () => {
//     console.log("Running Middleware");
// });

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}))
app.use('/products', productRoute);
app.use('/users', userRoute);
app.use('/sellers', sellerRoute);
app.use('/sales', salesRoute);

//ROUTES
app.get('/', (req, res) => {
    res.send("We are on home");
});


//Connect to DB
mongoose.connect(process.env.DB_CONNECTION,
{ useUnifiedTopology: true, useNewUrlParser: true  },
() => {
    console.log('Connected to DB');
})

//Listening to the server
app.listen(3000);