const mongoose = require('mongoose');

const sellerSchema = mongoose.Schema({
    sellerName: {
        type: String
    },
    sellerAddress: {
        type: String
    },
    isSellerApproved: {
        type: Boolean
    },
    sellerContactNo: {
        type : Number
    },
    sellerEmailId: {
        type: String,
    },
    sellerRegistrationDate: {
        type: Date
    },
    sellerPassword: {
        type: String
    },
    sellerApprovedDate: {
        type: Date,
        default: null
    }
});

module.exports = mongoose.model('Seller', sellerSchema);