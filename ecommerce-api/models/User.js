const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    userEmailId: {
        type: String,
    },
    userPassword: {
        type: String,
    },
    userAccessLevel: {
        type: Number
    },
    type: {
        type: String
    },
    userCartItems: {
        type: Object,
        default: {}
    },
    userOrders: {
        type: Object,
        default: {}
    }
});

module.exports = mongoose.model('User', userSchema);