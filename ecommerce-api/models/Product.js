const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    productId : {
        type: Number
    },
    productName: {
        type: String,
    },
    productPrice: {
        type: String,
    },
    availableStock: {
        type: String,
    },
    productShortDescription: {
        type: String,
        default:""
    },
    productDescription: {
        type: String,
        default:""
    },
    sellerName: {
        type: String
    },
    sellerId: {
        type: String
    }
});

module.exports = mongoose.model('Product', productSchema);