const mongoose = require('mongoose');

const salesSchema = mongoose.Schema({
    saleDate: {
        type: Date,
    },
    productId: {
        type: Number,
    },
    productName: {
        type: String,
    },
    productPrice: {
        type: String,
    },
    sellerName: {
        type: String,
    },
    sellerId: {
        type: String,
    },
    quantity: {
        type: String,
    },
    type: {
        type: String,
    }
});

module.exports =  mongoose.model('Sales', salesSchema);