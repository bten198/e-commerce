import React, { Fragment, useState, useEffect } from "react";
import { PropagateLoader } from 'react-spinners'

import SellerCard from './SellerCard';
import withContext from "../withContext";
import axiosUrl from "../axios/axios";
import '../styles/ProductList.css';

const GetSellers = (setSellers, setLoading, setError) => {
  axiosUrl.get('sellers').then(async response => {
    const jsonData = await response.data;
    await setSellers(jsonData);
    setInterval(() => {
      setLoading(false);
    }, 500)
  }).catch(err => {
    setInterval(() => {
      setLoading(false);
    }, 500)
    setError("Something Went Wrong");
  })
};

const SellersList = props => {
  const [sellers, setSellers] = useState(null);
  const [loading, setLoading] = useState(true);
  const [showApprovedSeller, setShowApprovedSeller] = useState(false);
  const [showPendingSeller, setShowPendingSeller] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    GetSellers(setSellers, setLoading, setError);
  }, []);

  const handleShowApproved = (key) => {
    switch(key){
      case "pending":
        return setShowPendingSeller(!showPendingSeller);
      case "approved":
        return setShowApprovedSeller(!showApprovedSeller);
      default:
        return;
    }
  }
  console.log("Sellers:::::",sellers);

  if(loading) return (
    <>
    <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">Our Products</h4>
        </div>
      </div>
      <div className="product-list-loader">
        <PropagateLoader 
          color="blue"
          loading={loading} 
        />
      </div>
    </>
  )

  return (  
    <Fragment>
      <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">Sellers List</h4>
        </div>
      </div>
      <br />
      <div className="container">
        <div className="button is-white is-size-4 is-spaced has-text-link" onClick={() => handleShowApproved("pending")}  >Pending for Approval</div>
        {showPendingSeller ? <div className="column columns is-multiline">
          {sellers && sellers.length ? (
            sellers.map((seller, index) => {
              if(seller.isSellerApproved === false)
              return <SellerCard
                seller={seller}
                key={index}
                isApproved={seller.isSellerApproved}
              />
              return "";
            }
            )) : (
            <div className="column">
              <span className="title has-text-grey-light">
                {error || "No Any Sellers Found"}
              </span>
            </div>
          )}
        </div> : null}
        <br></br>
        <div className="button is-white is-size-4 is-spaced color-blue has-text-link" onClick={() => handleShowApproved("approved")} >Approved Sellers</div>
        {showApprovedSeller ? <div className="column columns is-multiline">
          {sellers && sellers.length ? (
            sellers.map((seller, index) => {
              if(seller.isSellerApproved === true){
              return <SellerCard
                seller={seller}
                key={index}
                isApproved={seller.isSellerApproved}
              />
              }
              return null;
            }
            )) : (
            <div className="column">
              <span className="title has-text-grey-light">
                {error || "No Any Sellers Found"}
              </span>
            </div>
          )}
        </div> : null}
      </div>
    </Fragment>
  );
};

export default withContext(SellersList);
