import React, { Fragment, useState, useEffect } from "react";
import { PropagateLoader } from 'react-spinners'
import { useHistory } from 'react-router-dom';
import axiosUrl from "../axios/axios";

import withContext from "../withContext";
import '../styles/ProductList.css';
import ReportItem from "./ReportItem";

const ListWiseReport = props => {
    const history = useHistory();
    const data = history.location.state;
  const [allProducts, updateAllProducts] = useState([]);
  const [products, setProducts] = useState(data);
  const [loading, setLoading] = useState(true);
  const user = JSON.parse(localStorage.getItem("user"));

  useEffect(() => {
    axiosUrl.get(`/products/${user?.sellerId}`).then(response => {
      const jsonData = response.data;
      updateAllProducts(jsonData);
    })
    setTimeout(() => {
        setLoading(false)
    }, []);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const filterProducts = (key) => {
    console.log("filter", key)
    const filteredProduct = data.filter(product => product.type === key);
    console.log('filtered', filteredProduct)
    setProducts(filteredProduct);
  }

  const handleSort = (e) => {
    const value = e.target.value;
    console.log("called", value);
    switch(value){
      case 'purchase':
        return filterProducts("add");
      
      case 'sale':
        return filterProducts(value);
      
        default:
        return setProducts(data);
    }
  }

  const isProductRemoved = (id) => {
    let deleted = true;
    allProducts.map(product => {
      if(product.productId === id){
        return deleted = false;
      }
      return product;
    }
    );
    return deleted;
  }

  if(loading) return (
    <>
    <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">Detailed Report</h4>
        </div>
      </div>
      <div className="product-list-loader">
        <PropagateLoader 
          color="blue"
          loading={loading} 
        />
      </div>
    </>
  )

  return (  
    <Fragment>
      <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">Detailed Report</h4>
        </div>
      </div>
      <div className="sort">
      <b>Sort:</b> <select id="sort"  onChange={handleSort} >
        <option value="all">All</option>
        <option value="purchase">Purchase</option>
        <option value="sale" >Sale</option>
      </select>
      </div>
      <br />
      <div className="container">
        <div className="column columns is-multiline">
        {products.map(item => {
          return <ReportItem 
            report={item}
            isDeleted = {isProductRemoved(item.productId)}
          />
        })}
        </div>
      </div>
    </Fragment>
  );
};

export default withContext(ListWiseReport);
