import React, { Component, Fragment } from "react";
import { Link, Redirect } from "react-router-dom";
import { PuffLoader } from 'react-spinners'

import axiosUrl from "../axios/axios";
import withContext from "../withContext";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      loading: false
    };
  }
  handleChange = e =>
    this.setState({ [e.target.name]: e.target.value, error: "" });

  signup = async () => {
    this.setState({
        loading: true
    });

    const { username, password } = this.state;
    const regex = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/
    if(!regex.test(username)) {
      return this.setState({ error: "Enter Valid Email!!", loading: false });
    }
    if (!username || !password) {
      return this.setState({ error: "Fill all fields!", loading: false });
    }

    await axiosUrl.get('users').then(async response => {
      const data = await response.data;
      for(let user of data){
        if(username === user.userEmailId){
          this.setState({error: "Email Address or Username Already Registered", loading: false});
          return;
        }
      };

    await axiosUrl.post('users', {
            userEmailId: username,
            userPassword: password,
            userAccessLevel: 1,
            type: "regular"
        });
    this.setState({
        loading: false,
    });
    this.props.history.push("/login");
    }).catch(err => {
      this.setState({error: "Something Went Wrong! Check Internet Connectivity"})
    });
  };

  render() {
    return !this.props.context.user ? (
      <Fragment>
        <div className="hero is-primary ">
          <div className="hero-body container">
            <h4 className="title">Signup</h4>
          </div>
        </div>
        <br />
        <br />
        <div className="columns is-mobile is-centered">
          <div className="column is-one-third">
            <div className="field">
              <label className="label">Enter User Name or Email ID: </label>
              <input
                className="input"
                type="email"
                name="username"
                onChange={this.handleChange}
              />
            </div>
            <div className="field">
              <label className="label">Enter Password: </label>
              <input
                className="input"
                type="password"
                name="password"
                onChange={this.handleChange}
              />
            </div>
            {this.state.error && (
              <div className="has-text-danger">{this.state.error}</div>
            )}
            <div className="field is-clearfix">
            {!this.state.loading
            ? <button
                className="button is-primary is-outlined is-pulled-right"
                onClick={this.signup}
              >
                  Create Account
              </button>
            : <button
                className="button is-primary is-outlined is-pulled-right"
                onClick={this.signup}
                disabled
                >
                <PuffLoader color="blue" size="20" loading={this.state.loading} />
                </button>}
            </div>
            <Link className="tag is-link is-light" to="/signup">Don't Have Account? Create Account</Link> 
          </div>
        </div>
      </Fragment>
    ) : (
      <Redirect to="/products" />
    );
  }
}

export default withContext(Login);
