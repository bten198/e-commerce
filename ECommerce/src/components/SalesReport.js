/* eslint-disable react-hooks/exhaustive-deps */
import React, { Fragment, useState, useEffect } from "react";
import { BarLoader } from 'react-spinners'
import { PieChart } from 'react-minimal-pie-chart'
import { Link } from 'react-router-dom'

import withContext from "../withContext";
import axiosUrl from "../axios/axios";
import '../styles/salesReport.css';

const GetReport = ( sellerId, setReport, setLoading, setError) => {
  axiosUrl.get(`sales/${sellerId}`).then(async response => {
    const jsonData = await response.data;
    await setReport(jsonData);
    setInterval(() => {
      setLoading(false);
    }, 1000)
  }).catch(err => {
    setInterval(() => {
      setLoading(false);
    }, 1000)
    setError("Something Went Wrong");
  })
};

const SalesReport = props => {
  const [Report, setReport] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const user = JSON.parse(localStorage.getItem("user"));
  console.log(Report, error)

  useEffect(() => {
  GetReport(user?.sellerId, setReport, setLoading, setError);
  }, []);

  let totalInvestments = 0;
  let totalEarnings = 0;
  let totalPurchase = 0;
  let totalSales = 0;
  if(Report) {
  Report.forEach((item) => {
    console.log("ITEM:::",item);
      if(item.type === "sale"){
        totalEarnings += Number(item.productPrice * item.quantity);
        totalSales += 1;
      }else {
        totalInvestments += Number(item.productPrice * item.quantity);
        totalPurchase += 1;
      }
  })}

  const productChart = [
      {key: "Purchase", title: "Purchase", value: totalPurchase, color: "red"},
      {key: "Sales", title: "Sales", value: totalSales, color: "rgb(114, 180, 235)"}
  ]

  const salesChart = [
    {key: "Investment", title: "Investment", value: totalInvestments, color: "red"},
    {key: "Earning", title: "Earning", value: totalEarnings, color: "rgb(114, 180, 235)"}
  ]

  const pieChartStyle = {
      height: "20rem"
  };

  console.log("Total Sales", totalSales);

  if(loading) return (
    <>
    <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">Sales Report</h4>
        </div>
      </div>
      <div className="product-list-loader">
        <BarLoader 
          color="blue"
          loading={loading} 
        />
      </div>
    </>
  )

  return (  
    <Fragment>
      <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">Sales Report</h4>
        </div>
      </div>
      <br />
      <div className="container is-flex is-flex-direction-rows is-justify-content-space-between">
        <div className="title is-2  mx-2">
          Total Sales: <span className="has-text-danger mr-2">₹{totalEarnings}</span>
        </div>&nbsp;&nbsp;&nbsp;&nbsp;
        <div className="title is-2  mr-2">
          Total Investment: <span className="has-text-danger">₹{totalInvestments}</span>
        </div>&nbsp;&nbsp;&nbsp;&nbsp;
        <div className="title is-2">
          Total Profit: <span className="has-text-danger">₹{totalEarnings - totalInvestments}</span>
        </div>
      </div>&nbsp;&nbsp;&nbsp;&nbsp;
      <div className="container">
        <Link className="title has-text-link is-4" to={
            {
                pathname: "/detail-report",
                state: Report,
            }
        } >Chart for Products</Link>
        <hr></hr>
        <div className="salesReport-skyblue">Product Sold</div>
        <div className="salesReport-red">Product Purchased</div>
        <PieChart 
            animate={true} 
            animationDuration={200} 
            animationEasing={true} 
            data={productChart}
            lineWidth={60}
            paddingAngle={5}
            label={(data) => data.title}
            style={pieChartStyle} 
            totalValue={totalPurchase+totalSales} 
        />
        </div>
&nbsp;&nbsp;&nbsp;&nbsp;
        <div className="container">
        <Link className="title is-4 has-text-link " to={
            {
                pathname: "/detail-report",
                state: Report,
            }
        }>Chart for Purchase/Sales</Link>
        <hr></hr>
        <div className="salesReport-skyblue">Investments</div>
        <div className="salesReport-red">Earnings</div>
        <PieChart 
            animate={true} 
            animationDuration={200} 
            animationEasing={true} 
            data={salesChart}
            lineWidth={60}
            paddingAngle={5}
            label={(data) => data.title}
            style={pieChartStyle} 
            totalValue={totalEarnings+totalInvestments} 
        />
        </div>
    </Fragment>
  );
};

export default withContext(SalesReport);
