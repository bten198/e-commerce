import React, { Fragment } from "react";
import axiosUrl from '../axios/axios';
import withContext from "../withContext";
import CartItem from "./CartItem";

const Cart = props => {
  const { cart } = props.context;
  const cartKeys = Object.keys(cart || {});
  console.log(cart, cartKeys);

  const handleCheckout = async () => {
    const date = new Date();
    cartKeys.forEach(async (item) => {
      const product = cart[item];
      try {
      await axiosUrl.get(`/products/${product.product.productId}`).then(async response => {
        const jsonData = await response.data;
        const quantity = jsonData[0].availableStock;
        if(product.amount <= quantity){        
          await axiosUrl.patch(`products/${product.product.productId}/${quantity - product.amount}`);
          props.context.checkout()
        }
        else {
          throw new Error('Something Went Wrong');
        }
      });
      await axiosUrl.post('/sales', {
        saleDate: date.toISOString(),
        productId: product.product.productId,
        productName: product.product.productName,
        productPrice: product.product.productPrice,
        sellerName: product.product.sellerName,
        sellerId: product.product.sellerId,
        quantity: product.amount,
        type: "sale"
      });
      } catch(err) {
        console.log("Something Went Wrong");
      }
    });
  }

  return (
    <Fragment>
      <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">My Cart</h4>
        </div>
      </div>
      <br />
      <div className="container">
        {cartKeys.length ? (
          <div className="column columns is-multiline">
            {cartKeys.map(key => (
              <CartItem
                cartKey={key}
                key={key}
                cartItem={cart[key]}
                removeFromCart={props.context.removeFromCart}
              />
            ))}
            <div className="column is-12 is-clearfix">
              <br />
              <div className="is-pulled-right">
                <button
                  onClick={props.context.clearCart}
                  className="button is-warning "
                >
                  Clear cart
                </button>{" "}
                <button
                  className="button is-success"
                  onClick={handleCheckout}
                >
                  Checkout
                </button>
              </div>
            </div>
          </div>
        ) : (
          <div className="column">
            <div className="title has-text-grey-light">No item in cart!</div>
          </div>
        )}
      </div>
    </Fragment>
  );
};

export default withContext(Cart);
