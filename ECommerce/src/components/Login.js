import React, { Component, Fragment } from "react";
import withContext from "../withContext";
import { Link, Redirect } from "react-router-dom";
import axiosUrl from "../axios/axios";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }
  handleChange = e =>
    this.setState({ [e.target.name]: e.target.value, error: "" });

  login = async () => {
    const { username, password } = this.state;
    const regex = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/
    if(!regex.test(username) && username !== "admin") {
      return this.setState({ error: "Enter Valid Email!!", loading: false });
    }
    if (!regex.test(username) && (!username || !password)) {
      return this.setState({ error: "Fill all fields!" });
    }

    await axiosUrl.get('users').then(async response => {
      const data = await response.data;
      for(let user of data){
        if(username === user.userEmailId && password === user.userPassword){
          localStorage.setItem("user", JSON.stringify({
            username: username,
            accessLevel: user.userAccessLevel,
            type:user.type
          }));
          this.props.history.push('/');
          window.location.reload(true);
        }
      }
      this.setState({error: "Invalid Credentials"});
    }).catch(err => {
      this.setState({error: "Something Went Wrong! Check Internet Connectivity"})
    });
  };

  render() {
    return !this.props.context.user ? (
      <Fragment>
        <div className="hero is-primary ">
          <div className="hero-body container">
            <h4 className="title">Login</h4>
          </div>
        </div>
        <br />
        <br />
        <div className="columns is-mobile is-centered">
          <div className="column is-one-third">
            <div className="field">
              <label className="label">User Name: </label>
              <input
                className="input"
                type="email"
                name="username"
                onChange={this.handleChange}
              />
            </div>
            <div className="field">
              <label className="label">Password: </label>
              <input
                className="input"
                type="password"
                name="password"
                onChange={this.handleChange}
              />
            </div>
            {this.state.error && (
              <div className="has-text-danger">{this.state.error}</div>
            )}
            <div className="field is-clearfix">
              <button
                className="button is-primary is-outlined is-pulled-right"
                onClick={this.login}
              >
                Submit
              </button>
            </div>
            <Link className="tag is-link is-light" to="/signup">Don't Have Account? Create Account</Link><br></br>
            <br></br>
            <Link className="tag is-dark is-small" to="/seller-login">SignIn as a Seller</Link>
          </div>
        </div>
      </Fragment>
    ) : (
      <Redirect to="/products" />
    );
  }
}

export default withContext(Login);
