import React, { useState } from "react";
import {useHistory} from 'react-router-dom';

import axiosUrl from '../axios/axios';

const ProductItem = props => {
  const { product, enableRemove, showButton, editable = false, renderList = () => {}, handleUpdateQuantity = (id) => {} } = props;
  const [showEdit, updateShowEdit] = useState(false);
  const [quantity, updateQuantity] = useState(product.availableStock);
  const history = useHistory();

  const handleAddToCart = () => {
    if(localStorage.getItem("user")){
      props.addToCart({
      id: product.name || product.productName,
      product,
      amount: 1
    });
    return;
    }
    history.push("/login");
  }

  console.log(product);

  const handleQuantityChange = (e) => {
    updateQuantity(e.target.value);
  }

  const handleUpdate = async () => {
    const date = new Date();
    if(product.availableStock < quantity) {
    await handleUpdateQuantity(product.productId, quantity);
    axiosUrl.post('/sales', {
      saleDate: date.toISOString(),
      productId: product.productId,
      productName: product.productName,
      productPrice: product.productPrice,
      sellerName: product.sellerName,
      sellerId: product.sellerId,
      quantity: quantity - product.availableStock,
      type: "add"
    });
  }
    updateShowEdit(false);
  }

  const handleRemoveItem = async (id) => {
    await axiosUrl.delete(`products/${id}`)
    .then(response => {
      console.log(response);
    });
    renderList();
  }
  return (
    <div className=" column is-half">
      <div className="box">
        <div className="media">
          <div className="media-left">
            <figure className="image is-64x64">
              <img
                src="https://bulma.io/images/placeholders/128x128.png"
                alt={product.productName}
              />
            </figure>
          </div>
          <div className="media-content">
            <b style={{ textTransform: "capitalize" }}>
              {product.name || product.productName}{" "}
              <span className="tag is-primary">₹{product.price || product.productPrice}</span>
            </b>
            <div>{product.shortDesc || product.productShortDescription}</div>
            {!showEdit ?
            (product.stock || product.availableStock) > 0 ? (
              <small>{(product.stock || product.availableStock) + " Available"}</small>
            ) : (
              <small className="has-text-danger">Out Of Stock</small>
            ) : <input type="number" value={quantity} onChange={handleQuantityChange} /> }
            {showButton ?
            <div className="is-clearfix">
              {!enableRemove
              ?<button
                className="button is-small is-outlined is-primary   is-pulled-right"
                onClick={ handleAddToCart }
              >
                Add to Cart
              </button>
              :<button
              className="button is-small is-outlined is-secondary bg-danger   is-pulled-right"
              onClick={() => {
                handleRemoveItem(product._id);
              }}
            >
              Remove Product
            </button>}
            </div>
            :null}
            {editable ?
            !showEdit ? <button
                className="button is-small is-outlined is-primary   is-pulled-right"
                onClick={() => updateShowEdit(true)}
              >
                Update Quantity
              </button> : (<>&nbsp; &nbsp;<button
              className="button is-small is-outlined is-primary   is-pulled-right"
              onClick={() => updateShowEdit(false)}
            >
              Cancel
            </button>
            <button
                className="button is-small is-outlined is-primary   is-pulled-right"
                onClick={handleUpdate}
              >
                Save
              </button></>
              ) : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductItem;
