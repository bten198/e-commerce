import React, { Component, Fragment } from "react";
import withContext from "../withContext";
import { Redirect } from "react-router-dom";
import axiosUrl from "../axios/axios";

const initState = {
  name: "",
  price: "",
  stock: "",
  shortDesc: "",
  description: "",
  sellerName: "",
};

class AddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = initState;
  }

  save = e => {
    e.preventDefault();
    const { name, price } = this.state;
    const date = new Date();
    const { user } = this.props.context
    if (name && price) {
      // this.props.context.addProduct(
      //   {
      //     name,
      //     price,
      //     shortDesc,
      //     description,
      //     stock: stock || 0
      //   },
      //   () => this.setState(initState)
      // );
      const id = Math.floor(Math.random()*10000);
      axiosUrl.post('/products', {
        productId: id,
        productName: this.state.name,
        productPrice: this.state.price,
        availableStock: this.state.stock || "0",
        productShortDescription: this.state.shortDesc,
        productDescription: this.state.description,
        sellerName: user.sellerName,
        sellerId: user.sellerId,
      });
      
      //Add in Report
        axiosUrl.post('/sales', {
          saleDate: date.toISOString(),
          productId: id,
          productName: this.state.name,
          productPrice: this.state.price,
          sellerName: user.sellerName,
          sellerId: user.sellerId,
          quantity: this.state.stock,
          type: "add"
        });
      this.props.history.push('/seller-products');
    } else {
      this.setState({ error: "Please Enter name and price" });
    }
  };

  handleChange = e =>
    this.setState({ [e.target.name]: e.target.value, error: "" });

  render() {
    const { user } = this.props.context;
    return !(user && user.accessLevel < 1) ? (
      <Redirect to="/" />
    ) : (
      <Fragment>
        <div className="hero is-primary ">
          <div className="hero-body container">
            <h4 className="title">Login</h4>
          </div>
        </div>
        <br />
        <br />
        <form onSubmit={this.save}>
          <div className="columns is-mobile is-centered">
            <div className="column is-one-third">
              <div className="field">
                <label className="label">Product Name: </label>
                <input
                  className="input"
                  type="text"
                  name="name"
                  value={this.state.name}
                  onChange={this.handleChange}
                  required
                />
              </div>
              <div className="field">
                <label className="label">Price: </label>
                <input
                  className="input"
                  type="number"
                  name="price"
                  value={this.state.price}
                  onChange={this.handleChange}
                  required
                />
              </div>
              <div className="field">
                <label className="label">Available in Stock: </label>
                <input
                  className="input"
                  type="number"
                  name="stock"
                  value={this.state.stock}
                  onChange={this.handleChange}
                />
              </div>
              <div className="field">
                <label className="label">Short Description: </label>
                <input
                  className="input"
                  type="text"
                  name="shortDesc"
                  value={this.state.shortDesc}
                  onChange={this.handleChange}
                />
              </div>
              <div className="field">
                <label className="label">Description: </label>
                <textarea
                  className="textarea"
                  type="text"
                  rows="2"
                  style={{ resize: "none" }}
                  name="description"
                  value={this.state.description}
                  onChange={this.handleChange}
                />
              </div>
              {this.state.error && (
                <div className="error">{this.state.error}</div>
              )}
              <div className="field is-clearfix">
                <button
                  className="button is-primary is-outlined is-pulled-right"
                  type="submit"
                  onClick={this.save}
                >
                  Submit
                </button>
              </div>
            </div>
          </div>
        </form>
      </Fragment>
    );
  }
}

export default withContext(AddProduct);
