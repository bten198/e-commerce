import React from 'react';
import {Redirect, Route} from 'react-router-dom';

const ProtectedRoute = ({component: Component, ...rest}) => {
    const user = localStorage.getItem("user");
    return (
        <Route {...rest} render={(props) => {
            if(user){
                return <Component {...props} />
            }
            else {
                return <Redirect to={{
                    pathname: "/login",
                }} />
            }
        }} />
    )
};

export default ProtectedRoute;