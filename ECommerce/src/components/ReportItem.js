import React from "react";

const ReportItem = props => {
  const {report, isDeleted = false} = props;
console.log("REPORT:::", report)
  return (
    <div className=" column is-half">
      <div className="box">
        <div className="media">
          <div className="media-left">
          </div>
          <div className="media-content">
            <b style={{ textTransform: "capitalize" }}>
              {report.productName}{" "}
              <span className="tag is-primary">₹{report.productPrice}</span>
              {isDeleted ? <span className="has-text-danger" style={{ marginLeft: '20px'}} >Deleted</span> : null}
            </b>
            <div>{report.saleDate.substr(0,10)}</div>
              <small className="has-text-danger">{report.quantity}</small>
            <div className="is-clearfix">
             {report.sellerName}
            </div>
            <div className="is-clearfix">
             {report.type === "add"? "Purchase" : "Sale"}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReportItem;
