import React, { Fragment, useState, useEffect } from "react";
import { PropagateLoader } from 'react-spinners'

import ProductItem from "./ProductItem";
import withContext from "../withContext";
import axiosUrl from "../axios/axios";
import '../styles/ProductList.css';

const GetProducts = (sellerId, setProducts, setLoading, setError) => {
  axiosUrl.get(`products/${sellerId}`).then(async response => {
    const jsonData = await response.data;
    await setProducts(jsonData);
    setInterval(() => {
      setLoading(false);
    }, 500)
  }).catch(err => {
    setInterval(() => {
      setLoading(false);
    }, 500)
    setError("Something Went Wrong");
  })
};

const SellerProducts = props => {
  const [products, setProducts] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const user = JSON.parse(localStorage.getItem("user"));

  const renderList = () => {
    GetProducts(user?.sellerId, setProducts, setLoading, setError);
  }

  const handleUpdateQuantity = async (id, quantity) => {
    try {
    await axiosUrl.patch(`/products/${id}/${quantity}`);
    renderList();
    }catch(error) {
      console.log("Something Went Wrong");
    }
  }

  useEffect(() => {
    GetProducts(user?.sellerId, setProducts, setLoading, setError);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if(loading) return (
    <>
    <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">My Products</h4>
        </div>
      </div>
      <div className="product-list-loader">
        <PropagateLoader 
          color="blue"
          loading={loading} 
        />
      </div>
    </>
  )

  return (  
    <Fragment>
      <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">My Products</h4>
        </div>
      </div>
      <br />
      <div className="container">
        <div className="column columns is-multiline">
          {products && products.length ? (
            products.map((product, index) => (
              <ProductItem
                product={product}
                key={index}
                editable
                handleUpdateQuantity={handleUpdateQuantity}
                renderLis={renderList}
              />
            ))
          ) : (
            <div className="column">
              <span className="title has-text-grey-light">
                {error || "No Product Found"}
              </span>
            </div>
          )}
        </div>
      </div>
    </Fragment>
  );
};

export default withContext(SellerProducts);
