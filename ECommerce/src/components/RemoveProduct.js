import React, { Fragment, useState } from "react";
import { BarLoader } from 'react-spinners'

import ProductItem from "./ProductItem";
import withContext from "../withContext";
import axiosUrl from "../axios/axios";
import '../styles/ProductList.css';

const GetProducts = ( sellerId, setProducts, setLoading, setError) => {
  axiosUrl.get(`products/${sellerId}`).then(async response => {
    const jsonData = await response.data;
    await setProducts(jsonData);
    setInterval(() => {
      setLoading(false);
    }, 1000)
  }).catch(err => {
    setInterval(() => {
      setLoading(false);
    }, 1000)
    setError("Something Went Wrong");
  })
};

const RemoveProduct = props => {
  const [products, setProducts] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [getData, setGetData] = useState(true);
  const user = JSON.parse(localStorage.getItem("user"));
  
  GetProducts(user?.sellerId, setProducts, setLoading, setError);

  if(loading) return (
    <>
    <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">All Products</h4>
        </div>
      </div>
      <div className="product-list-loader">
        <BarLoader 
          color="blue"
          loading={loading} 
        />
      </div>
    </>
  )

  return (  
    <Fragment>
      <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">All Products</h4>
        </div>
      </div>
      <br />
      <div className="container">
        <div className="column columns is-multiline">
          {products && products.length ? (
            products.map((product, index) => (
              <ProductItem
                product={product}
                key={index}
                enableRemove={true}
                setData={setGetData}
                getData={getData}
                showButton={true}
              />
            ))
          ) : (
            <div className="column">
              <span className="title has-text-grey-light">
                {error || "No Product Found"}
              </span>
            </div>
          )}
        </div>
      </div>
    </Fragment>
  );
};

export default withContext(RemoveProduct);
