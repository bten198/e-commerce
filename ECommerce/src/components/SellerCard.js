import React, {useState} from "react";
import { PropagateLoader } from 'react-spinners'

import axiosUrl from '../axios/axios';
import Avatar from '../assets/img/avatar.jpg';

const ProductItem = props => {
  const { seller, isApproved } = props;
  const [loading, setLoading] = useState();

  const handleApproveSeller = () => {
    console.log(seller.sellerEmailId);
    setLoading(true);
    axiosUrl.patch(`sellers/${seller.sellerEmailId}`)
    .then(response => {
      setLoading(false);
      window.location.reload(true);
    })
    .catch(err => {
      setLoading(false);
      alert("Something Went Wrong");
    })
  }

  const handleDeleteSeller = async () => {
    setLoading(true);
    await axiosUrl.delete(`sellers/${seller.sellerEmailId}`)
    .then(response => {
      console.log(response);
      setLoading(false);
      window.location.reload(true);
    })
    .catch(err => {
      setLoading(false);
      window.location.reload(true);
    });
  }

  if(loading) return (
    <>
      <div className="product-list-loader">
        <PropagateLoader 
          color="blue"
          loading={loading} 
        />
      </div>
    </>
  )

  return (
    <div className=" column is-half">
      <div className="box">
        <div className="media">
          <div className="media-left">
            <figure className="image is-64x64">
              <img
                src={Avatar}
                alt={seller.sellerName}
              />
            </figure>
          </div>
          <div className="media-content">
            <b style={{ textTransform: "capitalize" }}>
              {seller.sellerName}
            </b>
            <div>{seller.sellerAddress}</div>
 
            <small>{`${seller.sellerContactNo}, ${seller.sellerEmailId}`}</small>
            <div className="is-clearfix">
              {!isApproved
              ?<><button
                className="button is-small is-outlined is-primary   is-pulled-right"
                onClick={ handleApproveSeller }
              >
                Approve
              </button>
              <button
              className="button m-2 mg-small is-small is-outlined is-secondary bg-danger   is-pulled-right"
              onClick={handleDeleteSeller }
                >
                Cancel Request
                </button></>
            :<button
              className="button is-small is-outlined is-secondary bg-danger   is-pulled-right"
              onClick={ handleDeleteSeller }
            >
              Delete Seller
            </button>}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductItem;
