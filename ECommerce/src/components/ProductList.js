import React, { Fragment, useState, useEffect } from "react";
import { PropagateLoader } from 'react-spinners'

import ProductItem from "./ProductItem";
import withContext from "../withContext";
import axiosUrl from "../axios/axios";
import '../styles/ProductList.css';

const GetProducts = (setProducts, setLoading, setError) => {
  axiosUrl.get('products').then(async response => {
    const jsonData = await response.data;
    await setProducts(jsonData);
    setInterval(() => {
      setLoading(false);
    }, 500)
  }).catch(err => {
    setInterval(() => {
      setLoading(false);
    }, 500)
    setError("Something Went Wrong");
  })
};

const ProductList = props => {
  const [products, setProducts] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    GetProducts(setProducts, setLoading, setError);
  }, []);

  if(loading) return (
    <>
    <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">Our Products</h4>
        </div>
      </div>
      <div className="product-list-loader">
        <PropagateLoader 
          color="blue"
          loading={loading} 
        />
      </div>
    </>
  )

  return (  
    <Fragment>
      <div className="hero is-primary">
        <div className="hero-body container">
          <h4 className="title">Our Products</h4>
        </div>
      </div>
      <br />
      <div className="container">
        <div className="column columns is-multiline">
          {products && products.length ? (
            products.map((product, index) => (
              <ProductItem
                product={product}
                key={index}
                addToCart={props.context.addToCart}
                showButton={true}
              />
            ))
          ) : (
            <div className="column">
              <span className="title has-text-grey-light">
                {error || "No Product Found"}
              </span>
            </div>
          )}
        </div>
      </div>
    </Fragment>
  );
};

export default withContext(ProductList);
