import React, { Component, Fragment } from "react";
import withContext from "../withContext";
import { Link, Redirect } from "react-router-dom";

import axiosUrl from "../axios/axios";

class SellerLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }
  handleChange = e =>
    this.setState({ [e.target.name]: e.target.value, error: "" });

  login = async () => {
    const { username, password } = this.state;
    const regex = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/
    if(!regex.test(username)) {
      return this.setState({ error: "Enter Valid Email!!", loading: false });
    }
    if (!username || !password) {
      return this.setState({ error: "Fill all fields!" });
    }

    await axiosUrl.get('sellers').then(async response => {
      const data = await response.data;
      for(let seller of data){
        if(username === seller.sellerEmailId && password === seller.sellerPassword){
          if(seller.isSellerApproved){
          localStorage.setItem("user", JSON.stringify({
            username: username,
            sellerName: `${seller.sellerName}`.toLowerCase(),
            sellerId: seller.sellerEmailId,
            accessLevel: 0,
            type: 'seller'
          }));
          this.props.history.push('/seller-products');
        }
        return this.setState({ error: "You are not Approved Yet"});
        }
      }
      this.setState({error: `Invalid Credentials`});
    }).catch(err => {
      this.setState({error: "Something Went Wrong! Check Internet Connectivity"})
    });
  };

  render() {
    return !this.props.context.user ? (
      <Fragment>
        <div className="hero is-primary ">
          <div className="hero-body container">
            <h4 className="title">Login As a Seller</h4>
          </div>
        </div>
        <br />
        <br />
        <div className="columns is-mobile is-centered">
          <div className="column is-one-third">
            <div className="field">
              <label className="label">User Name: </label>
              <input
                className="input"
                type="email"
                name="username"
                onChange={this.handleChange}
              />
            </div>
            <div className="field">
              <label className="label">Password: </label>
              <input
                className="input"
                type="password"
                name="password"
                onChange={this.handleChange}
              />
            </div>
            {this.state.error && (
              <div className="has-text-danger">{this.state.error}</div>
            )}
            <div className="field is-clearfix">
              <button
                className="button is-link is-outlined is-pulled-right"
                onClick={this.login}
              >
                Submit
              </button>
            </div> 
            <Link className="tag is-link is-light" to="/seller-signup">Register as a Seller</Link>
            <Link className="tag is-light is-small is-pulled-right" to="/login">SignIn as a Customer</Link>
          </div>
        </div>
      </Fragment>
    ) : (
      <Redirect to="/products" />
    );
  }
}

export default withContext(SellerLogin);
